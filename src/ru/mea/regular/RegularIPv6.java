package ru.mea.regular;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс, демонстрирующий проверку протокола IPv4 на валидность
 *
 * @author Е.А.Мешкова 18ИТ18
 */
public class RegularIPv6 {
    public static void main(String[] args) {
        int counter = 0;
        String ipv6 = "1:1:1:1:1:1:1:1";
        Pattern pattern = Pattern.compile(
                "^(" + "(\\p{XDigit}{1,4}:){7}\\p{XDigit}{1,4}|" + "(\\p{XDigit}{1,4}:){1,7}:|" +
                        "(\\p{XDigit}{1,4}:){1,6}(:(\\p{XDigit}{1,4}))|" +
                        "(\\p{XDigit}{1,4}:){1,5}(:(\\p{XDigit}{1,4})){1,2}|" +
                        "(\\p{XDigit}{1,4}:){1,4}(:(\\p{XDigit}{1,4})){1,3}|" +
                        "(\\p{XDigit}{1,4}:){1,3}(:(\\p{XDigit}{1,4})){1,4}|" +
                        "(\\p{XDigit}{1,4}:){1,2}(:(\\p{XDigit}{1,4})){1,5}|" +
                        "(\\p{XDigit}{1,4}:)(:(\\p{XDigit}{1,4})){1,6}|" +
                        "(:((:(\\p{XDigit}{1,4})){1,7})|:)|" +
                        "::(ffff(:0{1,4})?:)?((\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.){3}(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])|" +
                        "((\\p{XDigit}{1,4}):){1,4}:((\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.){3}(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])|" +
                        ")$");

        Matcher matcher = pattern.matcher(ipv6);
        while (matcher.find()) {
            counter++;
            System.out.println("Match found '" + ipv6.substring(matcher.start(), matcher.end()) +
                    "'. Starting is index " + matcher.start() +
                    " and ending at index " + matcher.end());
        }
        System.out.println("Matcher found: " + counter);
    }
}
