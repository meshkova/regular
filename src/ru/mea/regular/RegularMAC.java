package ru.mea.regular;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс, демонстрирующий проверку MAC адреса на валидность
 *
 * @author Е.А.Мешкова 18ИТ18
 */
public class RegularMAC {
    public static void main(String[] args) {
        int counter = 0;
        String mac = "15:11:14:15:17:10";
        Pattern pattern = Pattern.compile("^(((\\p{XDigit}{2})[:-]){5}\\p{XDigit}{2})$");

        Matcher matcher = pattern.matcher(mac);

        while (matcher.find()) {
            counter++;
            System.out.println("Match found '" + mac.substring(matcher.start(), matcher.end()) +
                    "'. Starting is index " + matcher.start() +
                    " and ending at index " + matcher.end());
        }
        System.out.println("Matcher found: " + counter);
    }
}
