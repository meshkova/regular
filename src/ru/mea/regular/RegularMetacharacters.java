package ru.mea.regular;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс, демонстрирующий метасимволы
 *
 * @author Е.А.Мешкова 18ИТ18
 */
public class RegularMetacharacters {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("^[a-e]"); // крышка, цирркумфлекс
        Matcher matcher1 = pattern.matcher(" f z y h j");
        System.out.println(matcher1.find());

        Pattern pattern2 = Pattern.compile("[a-e]$");
        Matcher matcher2 = pattern2.matcher("hs   y d u f  f");
        System.out.println(matcher2.find());

        Pattern pattern3 = Pattern.compile(".[0-9]");
        Matcher matcher3 = pattern3.matcher("123");
        System.out.println(matcher3.find());

        Pattern pattern4 = Pattern.compile("\\d");
        Matcher matcher4 = pattern4.matcher("123");
        System.out.println(matcher4.find());

        Pattern pattern5 = Pattern.compile(".[0-9]|.[a-c]");
        Matcher matcher5 = pattern5.matcher("123");
        System.out.println(matcher5.find());
        Matcher matcher6 = pattern5.matcher("hs   y d u f  f");
        System.out.println(matcher6.find());

        Pattern pattern6 = Pattern.compile("[^a-e]"); // поиск любого символа вне квадратных скобок
        Matcher matcher7 = pattern6.matcher("a");
        System.out.println(matcher7.find());


    }
}
