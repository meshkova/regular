package ru.mea.regular;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс, демонстрирующий использование захвата групп
 *
 * @author Е.А.Мешкова 18ИТ18
 */
public class RegularGroups {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("(\\d+)");
        Matcher matcher = pattern.matcher("2095 one 45 book year");
        while (matcher.find()) {
            System.out.println(matcher.group());
        }

        Pattern pattern1 = Pattern.compile("(\\d+).*\\1"); // "\\1"- Это обратная ссылка
        Matcher matcher1 = pattern1.matcher("2045 one 45 book year  45000.78");
        while (matcher1.find()) {
            System.out.println(matcher1.group());
        }
        Pattern pattern2 = Pattern.compile("(?:Mouse|Keyboard)"); // Группирует без запоминания сопоставленного текста.
        Matcher matcher2 = pattern2.matcher("2045 one 45 book year  45000.78");
        while (matcher2.find()) {
            System.out.println(matcher2.group());
        }

        Pattern pattern3 = Pattern.compile("Java (?=7|8)"); // Поиск наперёд, совпадения гне будут запоминаться
        Matcher matcher3 = pattern3.matcher("Java 7 8");
        while (matcher3.find()) {
            System.out.println(matcher3.group());
        }
        Pattern pattern4 = Pattern.compile("Java (?!7|8)"); // Поиск несоответствий наперёд
        Matcher matcher4 = pattern4.matcher("Java 7 8");
        while (matcher4.find()) {
            System.out.println(matcher4.group());
        }

    }


}
