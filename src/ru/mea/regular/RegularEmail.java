package ru.mea.regular;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс. демонстрирующий проверку на валидность E-mail
 *
 * @author Е.А.Мешкова 18ИТ18
 */
public class RegularEmail {
    public static void main(String[] args) {
        int counter = 0;
        String email = "example@gmail.com";
        Pattern pattern = Pattern.compile("^((\\w|[-+])+(\\.[\\w-]+)*@[\\w-]+((\\.[\\d\\p{Alpha}]+)*(\\.\\p{Alpha}{2,})*)*)$");
        Matcher matcher = pattern.matcher(email);

        while (matcher.find()) {
            counter++;
            System.out.println("Match found '" + email.substring(matcher.start(), matcher.end()) +
                    "'. Starting is index " + matcher.start() +
                    " and ending at index " + matcher.end());
        }
        System.out.println("Matcher found: " + counter);
    }
}
