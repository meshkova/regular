package ru.mea.regular;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс, демонстрирующий использование квантификаторов
 *
 * @author Е.А.Мешкова 18ИТ18
 */
public class RegularQuantifiers {
    public static void main(String[] args) {
        System.out.println(Pattern.compile("10{2}").matcher("100").find());

        // Ленивая квантификация
        int counter = 0;
        String str = "1.45 34543 3445";
        Pattern pattern = Pattern.compile(".*?45");
        Matcher matcher = pattern.matcher(str);
        while (matcher.find()) {
            counter++;
            System.out.println("Matcher found " + str.substring(matcher.start(), matcher.end()) +
                    " Starting at index " + matcher.start() +
                    " and ending at index " + matcher.end());

        }
        System.out.println("Matcher found: " + counter);

        // Жадная квантификация
        int counter2 = 0;
        String string = "1.45 34543  37 3445";
        Pattern pattern2 = Pattern.compile(".*45");
        Matcher matcher2 = pattern2.matcher(string);
        while (matcher2.find()) {
            counter2++;
            System.out.println("Matcher found " + string.substring(matcher2.start(), matcher2.end()) +
                    " Starting at index " + matcher2.start() +
                    " and ending at index " + matcher2.end());

        }

        System.out.println("Matcher found: " + counter2);
        //Ревнивая(сверхжадня) квантификация
        int counter3 = 0;
        String string2 = "1.45 34543  37 3445";
        Pattern pattern3 = Pattern.compile(".*+45");
        Matcher matcher3 = pattern3.matcher(string2);
        while (matcher3.find()) {
            counter3++;
            System.out.println("Matcher found " + string2.substring(matcher3.start(), matcher3.end()) +
                    " Starting at index " + matcher3.start() +
                    " and ending at index " + matcher3.end());

        }
        System.out.println("Matcher found: " + counter3);
    }
}
