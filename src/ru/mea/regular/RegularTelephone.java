package ru.mea.regular;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс, демонстрирующий проверку номера телефона на валидность
 *
 * @author Е.А.Мешкова 18ИТ18
 */
public class RegularTelephone {
    public static void main(String[] args) {
        String phoneNumber = "+79374329156";
        Pattern pattern = Pattern.compile("^((\\+79)([0-9]{9}))$");
        Matcher matcher = pattern.matcher(phoneNumber);
        if (matcher.matches()) {
            System.out.println("Phone number " + phoneNumber + " is correct");
        } else {
            System.out.println("Phone number " + phoneNumber + " no correct");
        }

    }
}
