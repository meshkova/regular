package ru.mea.regular;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс, демонстрирующий строковые литералы
 *
 * @author Е.А.Мешкова 18ИТ18
 */
public class RegularLiterals {
    public static void main(String[] args) {
        Pattern pattern1 = Pattern.compile("[a-z]+");
        Matcher matcher1 = pattern1.matcher("offrfh jj c c x a ");
        System.out.println(matcher1.find());

        Matcher matcher2 = pattern1.matcher(" 1 2 S B B  B G  R T E");
        System.out.println(matcher2.find());

        Pattern pattern2 = Pattern.compile("[a-zA-Z0-9]");
        Matcher matcher3 = pattern2.matcher(" 1 2 S B B  B G a R T E");
        System.out.println(matcher3.find());
    }
}
