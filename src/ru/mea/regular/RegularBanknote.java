package ru.mea.regular;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс, демонстрирующий проверку банковской карты на валидность
 *
 * @author Е.А.Мешкова 18ИТ18
 */
public class RegularBanknote {
    public static void main(String[] args) {
        String cardNumber = "4456678987651234";
        String date = "03/12";
        String cw = "123";
        Pattern patternCardNumber = Pattern.compile("(([2-6]([0-9]){3} ?)(([0-9]{4} ?){3}))");
        Pattern patternDate = Pattern.compile("(0[1-9]|1[0-2])/([0-9]{2})");
        Pattern patternCw = Pattern.compile("[0-9]{3}");

        Matcher matcherCardNumber = patternCardNumber.matcher(cardNumber);
        Matcher matcherDate = patternDate.matcher(date);
        Matcher matcherCw = patternCw.matcher(cw);

        if (matcherCardNumber.matches() && matcherDate.matches() && matcherCw.matches()) {
            System.out.println("Верно");
        } else {
            System.out.println("Неверно");
        }
    }

}
