package ru.mea.regular;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс, демонстрирующий символьные классы
 *
 * @author Е.А.Мешкова 18ИТ18
 */
public class RegularPosix {
    public static void main(String[] args) {
        int count = 0;
        String string = "17372  28nd   dyd637372. 23772";
        Pattern pattern = Pattern.compile("\\p{Digit}");
        Matcher matcher = pattern.matcher(string);

        while (matcher.find()) {
            count++;
            System.out.println("Matcher found " + string.substring(matcher.start(), matcher.end()) +
                    " Starting at index " + matcher.start() +
                    " and ending at index " + matcher.end());

        }
        System.out.println("Matcher found: " + count);

    }
}
